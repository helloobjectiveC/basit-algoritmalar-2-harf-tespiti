//
//  main.m
//  basitAlgoritmalar-2(kontrolYapılari)
//
//  Created by Yakup on 13.07.2017.
//  Copyright © 2017 Yakup. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        //harf tespiti...
        
        char karakter;
        NSLog(@"\nBir karakter Giriniz :"); scanf("%c",&karakter);
        
        
        if(isalpha(karakter))
            NSLog(@"Girilen Değer : %c bir karakterdir",karakter);
        else
            NSLog(@"Girilen Değer bir Karakter Değildir.");
        
    }
    return 0;
}
